// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGameGameModeBase.h"
#include "SnakeGameInstance.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "Pawns/SnakePawn.h"
#include "SnakeGame/Public/Core/Food.h"
#include "Pawns/SnakeElement.h"

void ASnakeGameGameModeBase::StartPlay()
{
	Super::StartPlay();

	SnakeInstance = Cast<USnakeGameInstance>(GetGameInstance());
	
	UE_LOG(LogTemp, Warning, TEXT("StartPlay"));
	
	SnakeElementSize = 110.f;
	
	if (IsValid(SnakeInstance))
    	{
    		OnMaxScoreUpdate.Broadcast(SnakeInstance->GetMaxScore());
    	}
	

}

void ASnakeGameGameModeBase::StartGame()
{
	OnStartGame.Broadcast();
	
	Player = Cast<ASnakePawn>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));

	if(Player)
	{
		SnakeElementSize = Player->GetSnakeSize();
	}
	FoodGenarator();
	
}


void ASnakeGameGameModeBase::RestartGame()
{
	
	if (IsValid(SnakeInstance))
	{
		SnakeInstance->SetMaxScore(CurrentScore);
	}
	
	CurrentScore = 0;
	OnReStartGame.Broadcast();

}

void ASnakeGameGameModeBase::AddScore(int32 ScoreToAdd)
{
	CurrentScore += ScoreToAdd;
	OnScoreUpdate.Broadcast(CurrentScore);
}

void ASnakeGameGameModeBase::SetMaxScore(int32 Score)
{
	if (IsValid(SnakeInstance))
	{
		SnakeInstance->SetMaxScore(CurrentScore);
		OnMaxScoreUpdate.Broadcast(SnakeInstance->GetMaxScore());
	}
}

int32 ASnakeGameGameModeBase::GetMaxScore() const
{
	if (IsValid(SnakeInstance))
	{
		return SnakeInstance->GetMaxScore();
	}
	return 0;
}

void ASnakeGameGameModeBase::FoodGenarator()
{
	if(IsValid(Food))
	{	
		int YOffset = FMath::RandRange(YEdge.X, YEdge.Y);
		int XOffset = FMath::RandRange(XEdge.X, XEdge.Y);
		FVector NewLocation;
		// Try to find free location
		for (int i = 0; i < 100; i++)
		{
			YOffset = FMath::RandRange(YEdge.X, YEdge.Y);
			XOffset = FMath::RandRange(XEdge.X, XEdge.Y);

			NewLocation = FVector(XOffset * SnakeElementSize, YOffset * SnakeElementSize, Player->GetActorLocation().Z);

			//Trace Objects Types
			TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
			traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

			// Ignore any specific actors
			TArray<AActor*> ignoreActors;

			// Ignore self or remove this line to not ignore any
			ignoreActors.Init(this, 1);

			TArray<AActor*> overlappedActor;

			// Class that the sphere should hit against and include in the outActors array (Can be null)
			UClass* seekClass = ASnakeElement::StaticClass(); // NULL;

			UKismetSystemLibrary::BoxOverlapActors(GetWorld(),
				NewLocation, FVector(SnakeElementSize), traceObjectTypes, seekClass, ignoreActors, overlappedActor);

			if (overlappedActor.Num() == 0)
			{
				const FTransform NewTransform = FTransform(NewLocation);
				AFood* NewFood = GetWorld()->SpawnActor<AFood>(Food, NewTransform);

				if (IsValid(NewFood))
					NewFood->SetGameMode(this);

				return;
			}
		}

		const FTransform NewTransform = FTransform(NewLocation);
		AFood* NewFood = GetWorld()->SpawnActor<AFood>(Food, NewTransform);

		if (IsValid(NewFood))
			NewFood->SetGameMode(this);
	}
}
