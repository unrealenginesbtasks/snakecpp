// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawns/SnakePawn.h"
#include "Pawns/SnakeElement.h"
#include "Components/InputComponent.h"
#include "SnakeGame/Public/Core/InteractiveObject.h"

// Sets default values
ASnakePawn::ASnakePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LastMovementDirection = EMovementDirection::EMD_UP;
	bIsSetDirection = false;
}

// Called when the game starts or when spawned
void ASnakePawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
}

// Called every frame
void ASnakePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

// Called to bind functionality to input
void ASnakePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if(!PlayerInputComponent)
		return;

	PlayerInputComponent->BindAxis("MoveRight", this, &ASnakePawn::MoveRight);
	PlayerInputComponent->BindAxis("MoveForward", this,  &ASnakePawn::MoveForward);
}

void ASnakePawn::MoveRight(float Value)
{
	if(bIsSetDirection)
		return;
	//UE_LOG(LogTemp, Warning, TEXT("MoveRight value %f"), Value);
	
	if(Value < 0 && LastMovementDirection != EMovementDirection::EMD_RIGHT )
	{
		LastMovementDirection = EMovementDirection::EMD_LEFT;
		bIsSetDirection = true;
	}
	else if(Value >0 && LastMovementDirection != EMovementDirection::EMD_LEFT)
	{
		LastMovementDirection = EMovementDirection::EMD_RIGHT;
		bIsSetDirection = true;
	}
}

void ASnakePawn::MoveForward(float Value)
{

	if(bIsSetDirection)
		return;
	
	//UE_LOG(LogTemp, Warning, TEXT("MoveForward value %f"), Value);
	if(Value < 0 && LastMovementDirection != EMovementDirection::EMD_UP)
	{
		LastMovementDirection = EMovementDirection::EMD_DOWN;
		bIsSetDirection = true;
	}
	else if(Value >0  && LastMovementDirection != EMovementDirection::EMD_DOWN)
	{
		LastMovementDirection = EMovementDirection::EMD_UP;
		bIsSetDirection = true;
	}
	 
}

void ASnakePawn::AddSnakeElement(int Elements)
{
	for(int i = 0; i< Elements; i++)
	{
				
		//FVector NewPosition = FVector(-SnakeElements.Num()*SnakeElementSize,GetActorLocation().Y,GetActorLocation().Z);
		FVector NewPosition;
		
		if(SnakeElements.Num() > 1)
		{
			FVector LastElemLocation = FVector(SnakeElements[SnakeElements.Num()-1]->GetActorLocation());
			NewPosition = LastElemLocation;
		}
		else
		{
			NewPosition = FVector(-SnakeElements.Num()*SnakeElementSize,GetActorLocation().Y,GetActorLocation().Z);
		}
		
		FTransform NewTransform = FTransform(NewPosition);
		ASnakeElement* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass,NewTransform);
		NewSnakeElement->SetSnakeElemOwner(this);
		const int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if(ElemIndex == 0)
		{
			NewSnakeElement->SetFirstSnakeElement();
		}
		if(ElemIndex == 1)
		{
			NewSnakeElement->DisableCollision();
		}
	}
}

void ASnakePawn::Move()
{
	if(!bCanMove)
		return;
	
	FVector MovementVector = FVector(ForceInitToZero) ;
	

	switch(LastMovementDirection)
	{
		case (EMovementDirection::EMD_UP):
		{
			MovementVector.X += SnakeElementSize;
			break;
		}
		case (EMovementDirection::EMD_DOWN):
		{
			MovementVector.X -= SnakeElementSize;
			break;
		}
		case (EMovementDirection::EMD_LEFT):
		{
			MovementVector.Y -= SnakeElementSize;
			break;
		}
		case (EMovementDirection::EMD_RIGHT):
		{
			MovementVector.Y += SnakeElementSize;
			break;
		}
	}
	
	
	for (int i = SnakeElements.Num()-1; i>0; i--)
	{
		const auto CurrentElement = SnakeElements[i];
		const auto PreviousElement = SnakeElements[i-1];
		FVector PrevLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector, false);
	
	bIsSetDirection = false;
}

void ASnakePawn::SnakeElemOverlap(ASnakeElement* Elem, AActor *Actor)
{
	//UE_LOG(LogTemp, Warning, TEXT("SnakeElemOverlap"));
	if(Elem)
	{
		if(SnakeElements.Find(Elem) == 0)
		{
			IInteractiveObject * InteractiveObject = Cast<IInteractiveObject>(Actor);
			
			if(InteractiveObject)
			{
				InteractiveObject->Interact(this);
			}
		}
	}
}